<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Windows</h1>
							
							<p>By your side from design to installation.</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Help Centre</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">		
			<div class="main-body">
			
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
				
				<aside class="sidebar">
					
					<a class="callout dark-bg" href="#" style="background-image: url(../assets/images/temp/request-a-quote.jpg);">
					
						<p>
							Get the effiiency, elegance and dependability you deserve.
						</p>
						
						<span class="big button">Request a Quote</span>
					</a><!-- .callout -->
					
				</aside><!-- .sidebar -->
				
			</div>
		</div><!-- .sw -->
	</section>
	
	<section>
	
		<div class="product-section">
		
			<div class="product-nav dark-bg">
				<div class="sw">
					<div class="nav-controls">
						<button class="t-fa-abs next">Next</button>
						<button class="t-fa-abs prev">Previous</button>
					</div><!-- .controls -->
					<div class="outer-product-nav">
					
						<div class="inner-product-nav">
						
							<!-- the HTML returned by the href of these links will be placed in div.product-content -->
							<a href="inc/i-sample-product-content.php" class="nav-element wsf w-awning">Awning</a>
							<a href="inc/i-sample-product-content.php" class="nav-element wsf w-casement">Casement</a>
							<a href="inc/i-sample-product-content.php" class="nav-element wsf w-slider">Slider</a>
							<a href="inc/i-sample-product-content.php" class="nav-element wsf w-single-hung">Single Hung</a>
							<a href="inc/i-sample-product-content.php" class="nav-element wsf w-bay">Bay</a>
							<a href="inc/i-sample-product-content.php" class="nav-element wsf w-bow">Bow</a>
							<a href="inc/i-sample-product-content.php" class="nav-element wsf w-picture">Picture</a>
							
						</div><!-- .inner-product-nav -->
						
					</div><!-- .outer-product-nav -->
				</div><!-- .sw -->
			</div><!-- .product-nav -->
			
			<div class="product-content with-loading-indicator">
			</div><!-- .product-content -->
			
		</div><!-- .product-section -->

	</section>
	
	<section>
		<div class="sw">
		
			<div class="section-title">
				<h2>Help Centre</h2>
				<span class="subtitle">Quality and craftsmanship from the beginning.</span>
			</div><!-- .section-title -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="split-screen dark-bg center collapse-1000">
		<div class="sw">
			<div class="grid ss eqh collapse-1000">
				<div class="col col-2">
					<a class="item" href="#">
					
						<div class="section-title">
							<h2>Installation</h2>
							<span class="subtitle">Subtitle</span>
						</div><!-- .section-title -->
						
						<div class="section-excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
							Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
						</div><!-- .section-excerpt -->
						
						<div class="center">
							<span class="button big">Read More</span>
						</div><!-- .center -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<a class="item" href="#">
						
							<div class="section-title">
								<h2>Warranties</h2>
								<span class="subtitle">Subtitle</span>

							</div><!-- .section-title -->
							
							<div class="section-excerpt">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
							</div><!-- .section-excerpt -->
							
							<div class="center">
								<span class="button big">Read More</span>
							</div><!-- .center -->
							
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .split-screen -->


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>