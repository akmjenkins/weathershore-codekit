<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Help Centre</h1>
							
							<p>Sub Title</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Help Centre</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
			<div class="main-body">
			
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
				
			</div><!-- .main-body -->
		</div><!-- .sw -->
	</section>
	
	<hr />
	
	<section class="help-blocks-wrap">
		<div class="sw">
		
			<div class="help-blocks">
				
				<div class="blocks-col">
				
					<div class="help-block">
						<h3 class="title">Contact Support</h3>
						<p>If you need help with anything, some is here to help. Use one of the options below to contact us.</p>
						
						<span class="phone">
							<i class="fa fa-phone"></i>
							709 753-7640
						</span><!-- .phone -->
						
						<a href="#" class="big button">
							<i class="fa fa-envelope"></i> E-mail Support
						</a>
					</div><!-- .help-block -->
					
					<div class="help-block">
						<h3 class="title">Installation Guides</h3>
						<p>Get all the information you need to install Weather Shore Windows in your home. Installation service also available.</p>
						
						<a href="#" class="big button">View More Information</a>
					</div><!-- .help-block -->
				
				</div><!-- .blocks-col -->
				
				<div class="blocks-col">
				
					<div class="help-block">
						<h3 class="title">Warranty</h3>
						<p>We believe our product is the best choice for your home. So we back it up to give you peace of mind.</p>
						
						<a href="#" class="big button">Warranty Prior to 2011</a>
						<a href="#" class="big button">WSW Warranty Card</a>
					</div><!-- .help-block -->
					
					<div class="help-block">
						<h3 class="title">Energy Efficiency</h3>
						<p>Our windows are manufactured to meet the toughest energey efficiency ratings.</p>
						
						<a href="#" class="big button">View More Information</a>
					</div><!-- .help-block -->
				
				</div><!-- .blocks-col -->
				
				<div class="help-form-wrap">
					
					<h3 class="title">Request a Service Call</h3>
					<p>If you have any issues or concerns with your Weather Shore product, please let us know and we will provide assistance as soon as possible!</p>
					
					<small>*Please allow 6-10 business days for the service work to be completed.</small>
					
					<form action="/" class="body-form">
						<fieldset>
							
							<input type="text" name="name" placeholder="Full Name">
							<input type="email" name="email" placeholder="Email">
							<input type="tel" pattern="\d+" name="phone" placeholder="Phone">
							<textarea name="message" placeholder="Tell us about your problem"></textarea>
							
							<button type="submit" class="big button">Submit</button>
						</fieldset>
					</form><!-- .body-form -->
					
				</div><!-- .help-form-wrap -->
				
				
				
			</div><!-- .help-blocks -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="split-screen dark-bg center collapse-1000">
		<div class="sw">
			<div class="grid ss eqh collapse-1000">
				<div class="col col-2">
					<a class="item" href="#">
					
						<div class="section-title">
							<h2>Window Operation</h2>
							<span class="subtitle">Learn correct window operation.</span>
						</div><!-- .section-title -->
						
						<div class="section-excerpt">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
							Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
						</div><!-- .section-excerpt -->
						
						<div class="center">
							<span class="button big">Read More</span>
						</div><!-- .center -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<a class="item" href="#">
						
							<div class="section-title">
								<h2>Door Operation</h2>
								<span class="subtitle">Learn correct window operation.</span>

							</div><!-- .section-title -->
							
							<div class="section-excerpt">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
							</div><!-- .section-excerpt -->
							
							<div class="center">
								<span class="button big">Read More</span>
							</div><!-- .center -->
							
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .split-screen -->
	
	<section>
		<div class="sw">
		
			<div class="section-title">
				<h2>Care &amp; Maintenance</h2>
				<span class="subtitle">Learn correct window operation.</span>
			</div><!-- .section-title -->
			
			<div class="section-excerpt">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
				Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
			</div><!-- .section-excerpt -->
			
			<div class="center">
				<a href="#" class="button big">Read More</a>
			</div><!-- .center -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg green-bg">
		<div class="sw">
		
			<div class="grid vcenter pad40 collapse-900">
				<div class="col col-2">
					<div class="item">
						
						<h3>Troubleshooting</h3>
						
						<p>
							Have questions about your Weather Shore windows, doors or exterior items? Please type a question or use one of the links below.
						</p>
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<div class="item">
					
						<form action="/" class="single-form" method="get" id="question-form">
							<span class="count">15</span>
							<fieldset>
								<input type="text" name="question" placeholder="Type your question here...">
								<button class="fa-search" title="Search">Search</button>
							</fieldset>
						</form><!-- .single-form -->
					
					</div><!-- .item -->
				</div><!-- .col -->
			</div>
		
		</div><!-- .sw -->
	</section><!-- .green-bg -->
	
	<section>
		<div class="sw">
		
			<div class="section-title">
				<h2>Frequently Asked Questions</h2>
				<span class="subtitle">Find out what people are asking.</span>
			</div><!-- .section-title -->
			
			<div class="section-excerpt">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
				Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
			</div><!-- .section-excerpt -->
			
			<div class="center">
				<a href="#" class="button big">Read More</a>
			</div><!-- .center -->
			
			
			<div class="accordion faq-accordion">
			
				<div class="accordion-item">
					<div class="accordion-item-handle">
						Sed facilisis nisl vel mi maximus, at varius sem auctor donec fringilla leo vitae justo tempus, et ullamcorper risus consectetur?
					</div><!-- .accordion-item-handle -->
					<div class="accordion-item-content">
					
						<p>
							Integer sed pellentesque tortor. Praesent id massa non neque pretium finibus et ac nisi. Curabitur eleifend eget mi in faucibus. 
							Praesent fermentum fringilla neque, eget sollicitudin mi dictum id. Vivamus pretium ultricies tortor ut tempus. Vivamus rhoncus, 
							magna eu hendrerit dictum, mi risus tempor tellus, id ultricies nunc ipsum id nulla. Quisque non velit vitae felis mattis pharetra non 
							eget nisi. Nam finibus urna vel turpis pretium ultrices. Nullam vestibulum nisi orci, in ullamcorper lorem tincidunt at.
						</p>
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
				<div class="accordion-item">
					<div class="accordion-item-handle">
						Sed facilisis nisl vel mi maximus, at varius sem auctor donec fringilla leo vitae justo tempus, et ullamcorper risus consectetur?
					</div><!-- .accordion-item-handle -->
					<div class="accordion-item-content">
					
						<p>
							Integer sed pellentesque tortor. Praesent id massa non neque pretium finibus et ac nisi. Curabitur eleifend eget mi in faucibus. 
							Praesent fermentum fringilla neque, eget sollicitudin mi dictum id. Vivamus pretium ultricies tortor ut tempus. Vivamus rhoncus, 
							magna eu hendrerit dictum, mi risus tempor tellus, id ultricies nunc ipsum id nulla. Quisque non velit vitae felis mattis pharetra non 
							eget nisi. Nam finibus urna vel turpis pretium ultrices. Nullam vestibulum nisi orci, in ullamcorper lorem tincidunt at.
						</p>
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
				<div class="accordion-item">
					<div class="accordion-item-handle">
						Sed facilisis nisl vel mi maximus, at varius sem auctor donec fringilla leo vitae justo tempus, et ullamcorper risus consectetur?
					</div><!-- .accordion-item-handle -->
					<div class="accordion-item-content">
					
						<p>
							Integer sed pellentesque tortor. Praesent id massa non neque pretium finibus et ac nisi. Curabitur eleifend eget mi in faucibus. 
							Praesent fermentum fringilla neque, eget sollicitudin mi dictum id. Vivamus pretium ultricies tortor ut tempus. Vivamus rhoncus, 
							magna eu hendrerit dictum, mi risus tempor tellus, id ultricies nunc ipsum id nulla. Quisque non velit vitae felis mattis pharetra non 
							eget nisi. Nam finibus urna vel turpis pretium ultrices. Nullam vestibulum nisi orci, in ullamcorper lorem tincidunt at.
						</p>
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
				<div class="accordion-item">
					<div class="accordion-item-handle">
						Sed facilisis nisl vel mi maximus, at varius sem auctor donec fringilla leo vitae justo tempus, et ullamcorper risus consectetur?
					</div><!-- .accordion-item-handle -->
					<div class="accordion-item-content">
					
						<p>
							Integer sed pellentesque tortor. Praesent id massa non neque pretium finibus et ac nisi. Curabitur eleifend eget mi in faucibus. 
							Praesent fermentum fringilla neque, eget sollicitudin mi dictum id. Vivamus pretium ultricies tortor ut tempus. Vivamus rhoncus, 
							magna eu hendrerit dictum, mi risus tempor tellus, id ultricies nunc ipsum id nulla. Quisque non velit vitae felis mattis pharetra non 
							eget nisi. Nam finibus urna vel turpis pretium ultrices. Nullam vestibulum nisi orci, in ullamcorper lorem tincidunt at.
						</p>
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
			</div><!-- .accordion -->
			
		</div><!-- .sw -->
	</section>


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>