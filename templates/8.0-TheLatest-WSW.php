<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">The Latest</h1>
							
							<p>Subtitle.</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">The Latest</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
			<div class="main-body">
			
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
				
			</div><!-- .main-body -->
		</div><!-- .sw -->
	</section>
	
	<hr />
	
	<section>
		<div class="sw">

			<div class="section-title l">
				<h2>Promotions</h2>
				<span class="subtitle">Sed Efficitur Tortor Tisus sed Ghsest.</span>
			</div><!-- .section-title -->
		
		</div><!-- .sw -->
		
		<div class="filter-area">
		
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
					<div class="count">
						<span class="num">15</span>
						Promotions Found
					</div><!-- .count -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
				<div class="sw">
					
					<div class="grid eqh collapse-at-850 blocks">
					
						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button" href="#">
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="time">
											
											<time datetime="2014-06-15">
											
												<span class="tag">Ends</span>
												
												<span class="day">15</span>
												
												<span class="month-year">
													<span class="month">Jun</span>
													<span class="year">2014</span>
												</span><!-- .month-year -->
												
											</time>
											
										</div><!-- .time -->
									
										<div class="title-block">
											<span class="h4-style heading title">This is a promotion title</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button" href="#">
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/images/temp/overview/block-1.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="time">
											
											<time datetime="2014-06-15">
											
												<span class="tag">Ends</span>
											
												<span class="day">15</span>
												
												<span class="month-year">
													<span class="month">Jun</span>
													<span class="year">2014</span>
												</span><!-- .month-year -->
												
											</time>
											
										</div><!-- .time -->
									
										<div class="title-block">
											<span class="h4-style heading title">This is a promotion title</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

					</div><!-- .grid -->

					
				</div><!-- .sw -->
			</div><!-- .filter-content -->
			
		</div><!-- .filter-area -->
		
	</section>

	<section>
		
		<div class="filter-area">
		
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
					<div class="count">
						<span class="num">6</span>
						Articles Found
					</div><!-- .count -->

				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
				<div class="sw">
					
					<div class="grid eqh collapse-at-850 blocks">
					
						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button" href="#">
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/images/temp/overview/ov9.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="time">
											
											<time datetime="2014-06-15">
												<span class="day">15</span>
												
												<span class="month-year">
													<span class="month">Jun</span>
													<span class="year">2014</span>
												</span><!-- .month-year -->
												
											</time>
											
										</div><!-- .time -->
									
										<div class="title-block">
											<span class="h4-style heading title">This is a news</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button" href="#">
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/images/temp/overview/ov10.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="time">
											
											<time datetime="2014-06-15">
												<span class="day">15</span>
												
												<span class="month-year">
													<span class="month">Jun</span>
													<span class="year">2014</span>
												</span><!-- .month-year -->
												
											</time>
											
										</div><!-- .time -->
									
										<div class="title-block">
											<span class="h4-style heading title">This is a news</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button" href="#">
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/images/temp/overview/block-2.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="time">
											
											<time datetime="2014-06-15">
												<span class="day">15</span>
												
												<span class="month-year">
													<span class="month">Jun</span>
													<span class="year">2014</span>
												</span><!-- .month-year -->
												
											</time>
											
										</div><!-- .time -->
									
										<div class="title-block">
											<span class="h4-style heading title">This is a news</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						
					</div><!-- .grid -->
					
				</div><!-- .sw -->
			</div><!-- .filter-content -->
			
		</div><!-- .filter-area -->
		
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>