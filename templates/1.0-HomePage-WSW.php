<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-hero-home.php'); ?>

<div class="body">
	
	<section class="bg with-img light-bg" style="background-image: url(../assets/images/temp/section-bg.jpg);">
		<div class="sw">
		
			<div class="section-title">
				<span class="h1-style">Our Story</span>
			</div><!-- .section-title -->
			
			<div class="section-excerpt">
				<p>A storied past and an even brighter future.</p>
			</div><!-- .content -->
			
			<div class="center">
				<a href="#" class="button big">Find Out More</a>
			</div><!-- .center -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="split-screen dark-bg center collapse-1000">
		<div class="sw">
			<div class="grid ss eqh collapse-1000">
				<div class="col col-2">
					<div class="item">
					
						<div class="section-title">
							<h2>Why Us?</h2>
							<span class="subtitle">Reliable quality. Made right here.</span>
						</div><!-- .section-title -->
						
						<div class="section-excerpt">
							<p>Sed elit ante, fringilla id magna a, sagittis vestibulum nisi. Maecenas semper turpis aliquam quam consequat lobortis. Praesent quis finibus nibh.</p>
						</div><!-- .section-excerpt -->
						
						<div class="center">
							<a href="#" class="button big">Read More</a>
						</div><!-- .center -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<div class="item">
						
							<div class="section-title">
								<h2>Showcase</h2>
								<span class="subtitle">See the Difference.</span>
							</div><!-- .section-title -->
							
							<div class="section-excerpt">
								<p>Sed elit ante, fringilla id magna a, sagittis vestibulum nisi.</p>
							</div><!-- .section-excerpt -->
							
							<div class="btn-group">
							
								<a href="#" class="button wsf windows bb">
									Windows
								</a>
								
								<a href="#" class="button wsf doors bb">
									Doors
								</a>
								
								<a href="#" class="button wsf exterior bb">
									Exterior
								</a>
							
							</div><!-- .btn-group -->

							
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .split-screen -->
	
	<section>
		<div class="sw">
		
			<div class="section-title">
				<h2>Help Centre</h2>
				<span class="subtitle">We are Always Here to Serve You.</span>
			</div><!-- .section-title -->
			
			<div class="section-excerpt">
				<p>Sed elit ante, fringilla id magna a, sagittis vestibulum nisi. Maecenas semper turpis aliquam quam consequat lobortis. Praesent quis finibus nibh.</p>
			</div><!-- .section-excerpt -->
			
			<div class="center">
				<div class="btn-group">
					
					<a href="#" class="button bb t-fa fa-phone">
						Call 709.753.7640
					</a><!-- .button -->
					
					<a href="#" class="button bb t-fa fa-envelope">
						Email Support
					</a><!-- .button -->
					
					<a href="#" class="button bb t-fa fa-info-circle">
						More Support
					</a><!-- .button -->
					
				</div><!-- .btn-griup -->
			</div>
			
			
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>