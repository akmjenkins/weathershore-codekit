			<footer>
				<div class="sw">

					<div class="footer-wrap">
				
						<div class="footer-contact">
							
							<div>
								<h6>Social</h6>
								<?php include('inc/i-social.php'); ?>
							</div>
							
							<div class="address-hours">
								<div>
									<h6>Showroom</h6>
									
									<address>
										Weather Shore Windows Inc. <br />
										77 Blackmarsh Road <br />
										St. John's, NL A1E 1S6
									</address>
									
									<span class="block">Tel: (709) 753-7640</span>
									<span class="block">Tel: (709) 753-6264</span>
								</div>
								
								<div>
									<h6>Store Hours</h6>
									
									<span class="block">Monday &mdash; Friday  9am - 9pm</span>
									<span class="block">Saturday &mdash; 9am - 5pm</span>
									<span class="block">Sunday &mdash; Closed</span>
								</div>
							</div>

						</div><!-- .footer-contact -->
						
						<div class="footer-nav-wrap">
							
							<div class="footer-nav">
								
								<ul>
								
									<li>
										<ul>
											<li><a href="#">Windows</a></li>
											<li><a href="#">Awning</a></li>
											<li><a href="#">Casement</a></li>
											<li><a href="#">Slider</a></li>
											<li><a href="#">Single Hung</a></li>
											<li><a href="#">Bay</a></li>
											<li><a href="#">Bow</a></li>
											<li><a href="#">Picture</a></li>
										</ul>
									</li>
									
									
									<li>
										<ul>
											<li><a href="#">Doors</a></li>
											<li><a href="#">Entry Doors</a></li>
											<li><a href="#">Patio Doors</a></li>
											<li><a href="#">Features &amp; Options</a></li>
											<li><a href="#">Door Installation</a></li>
										</ul>
									</li>
									
									<li>
										<ul>
											<li><a href="#">Exterior</a></li>
											<li><a href="#">Columns</a></li>
											<li><a href="#">Railings</a></li>
											<li><a href="#">Decking</a></li>
											<li><a href="#">Fencing</a></li>
											<li><a href="#">Pergolas</a></li>
											<li><a href="#">Siding</a></li>
										</ul>
									</li>
									
									<li>
										<ul>
											<li><a href="#">Why Us?</a></li>
											<li><a href="#">Our Story</a></li>
											<li><a href="#">Our Commitment</a></li>
											<li><a href="#">Customer Experience</a></li>
											<li><a href="#">Contact Us</a></li>
										</ul>
									</li>
									
									<li>
										<ul>
											<li><a href="#">Showcase</a></li>
											<li><a href="#">Windows</a></li>
											<li><a href="#">Doors</a></li>
											<li><a href="#">Exterior</a></li>
										</ul>
									</li>
									
									<li>
										<ul>
											<li><a href="#">Help Centre</a></li>
											<li><a href="#">Contact Support</a></li>
											<li><a href="#">Request a Service Call</a></li>
											<li><a href="#">Resources</a></li>
										</ul>
									</li>
									
								</ul>
								
								<a href="#" class="dark button" id="contactor-login">Contractor Login</a>
								
							</div><!-- .footer-nav -->
						
							<div class="copyright">
							
								<ul>
									<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Weather Shore Windows</a></li>
									<li><a href="#">Sitemap</a></li>
									<li><a href="#">Legal</a></li>
								</ul>
								
								<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
							</div><!-- .copyright -->
						
						</div><!-- .footer-nav-wrap -->
					
					</div><!-- .footer-wrap -->
					
				</div><!-- .sw -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
		
		<?php include('inc/i-build-price.php'); ?>
		<?php include('inc/i-search-overlay.php'); ?>
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/weather',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>