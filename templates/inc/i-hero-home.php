<div class="hero">
	<div class="swiper-wrapper">
		<div class="swipe" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<span class="title h1-style">Quality</span>
							
							<p>
								Manufactured to meet the highest standards of quality and efficiency.
							</p>
							
							<a href="#" class="button big">Find Out More</a>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>
				
				<div data-src="../assets/images/temp/hero/hero-2.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<span class="title h1-style">Lorem Ipsum</span>
							
							<p>
								Manufactured to meet the highest standards of quality and efficiency.
							</p>
							
							<a href="#" class="button big">Find Out More</a>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->