<div class="mobile-nav-bg"></div>
<div class="nav-wrap">

	<div class="nav">
		<div class="sw">
		
			<nav>
				<ul>
					<li class="drop">
						<a href="#" class="wsf windows">
							<span>Windows</span>
						</a>
						<div>
							
							<div class="meta-content">
							
								<span class="title">Explore</span>
								<span class="subtitle">Window Types</span>
								
								<p class="detail">
									At Weather Shore we share a common vision: to create windows that are as aesthetically pleasing as they are reliable.
								</p>
								
								<ul class="links">
									<li><a href="#">Feature &amp; Options</a></li>
									<li><a href="#">Customize Your Window</a></li>
									<li><a href="#">Window Installation</a></li>
									<li><a href="#">St. John's Heritage Code</a></li>
									<li><a href="#">Commerical Windows</a></li>
								</ul><!-- .links -->
								
							</div><!-- .meta-content -->
							
							<div class="swiper-wrapper button-controls">
								<div class="swipe" data-controls="true" data-links="true">
									<div class="swipe-wrap">
										<div>
										
											<ul>
												<li>
													<a href="#" class="wsf w-awning">Awning</a>
												</li>
												<li>
													<a href="#" class="wsf w-casement">Casement</a>
												</li>
												<li>
													<a href="#" class="wsf w-slider">Slider</a>
												</li>
												<li>
													<a href="#" class="wsf w-single-hung">Single Hung</a>
												</li>
											</ul>
										
										</div>
										
										<div>
										
											<ul>
												<li>
													<a href="#" class="wsf w-bay">Bay</a>
												</li>
												<li>
													<a href="#" class="wsf w-bow">Bow</a>
												</li>
												<li>
													<a href="#" class="wsf w-picture">Picture</a>
												</li>
											</ul>
										
										</div>
										
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->
							
						</div>
					</li>
					
					<li class="drop">
						<a href="#" class="wsf doors">
							<span>Doors</span>
						</a>
					</li><!-- .drop -->
					
					<li class="drop">
						<a href="#" class="wsf exterior">
							<span>Exterior</span>
						</a>
					</li><!-- .drop -->
					
				</ul>
			</nav>
		
			<div class="nav-top">
			
				<button class="toggle-search-form t-fa t-fa-abs fa-search">Search</button>
			
				<div class="nav-meta-nav">
					<a href="#">Services</a>
					<a href="#">The Latest</a>
					<a href="#">Contact</a>
				</div><!-- .meta-nav -->
				
				<?php include('inc/i-social.php'); ?>

			</div><!-- .top -->
			
		</div><!-- .sw -->
	</div><!-- .nav -->
</div><!-- .nav-wrap -->