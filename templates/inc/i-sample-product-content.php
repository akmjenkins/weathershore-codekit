<div class="product">
	
	<div class="sw">
		<div class="section-title l">
			<h2>Awning Windows</h2>
		</div>
	</div><!-- .sw -->
	
	<div class="tab-wrapper">
	
		<div class="green-bg tab-controls">
			<div class="sw">
				
				<div class="product-tab-nav">
				
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>Overview</option>
							<option>Specifications</option>
							<option>Features &amp; Options</option>
							<option>Build &amp; Price</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
			
					<div class="product-nav-element tab-control selected">
						<span class="t-fa fa-navicon">Overview</span>
					</div><!-- .product-nav-element -->
					
					<div class="product-nav-element tab-control">
						<span class="t-fa fa-wrench">Specifications</span>
					</div><!-- .product-nav-element -->
					
					<div class="product-nav-element tab-control">
						<span class="t-fa fa-cog">Features &amp; Options</span>
					</div><!-- .product-nav-element -->
					
					<div class="product-nav-element tab-control">
						<span class="t-fa fa-usd">Build &amp; Price</span>
					</div><!-- .product-nav-element -->
				
				</div><!-- .product-swiper-nav -->
			</div><!-- .sw -->
		</div><!-- .tab-controls -->
		
		<div class="tab-holder">
			
			<div class="tab selected">
				<div class="sw">
				
					<div class="grid vcenter collapse-750">
						<div class="col col-2">
							<div class="item">
							
								<h4 class="serif">Superior build quality with convenient features to give your home the comfort you need.</h4>
								
								<p>Weather Shore Awning windows can be custom made and have a special top-hinged, outward-opening design. 
								When open on rainy days, the Awning window will keep water out, and when closed, wind against the window glass 
								will cause the window to seal even tighter.</p>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-2 first">
							<div class="item">
								
								<div class="photoviewer">
									
									<div class="thumbs">
										<div class="ar" data-ar="100">
											<div class="ar-child lazybg" data-src="../assets/images/temp/overview/ov9.jpg"></div>
										</div>
										
										<div class="ar" data-ar="100">
											<div class="ar-child lazybg" data-src="../assets/images/temp/overview/ov8.jpg"></div>
										</div>
										
										<div class="ar" data-ar="100">
											<div class="ar-child lazybg" data-src="../assets/images/temp/overview/ov10.jpg"></div>
										</div>
										
										<div class="ar" data-ar="100">
											<div class="ar-child lazybg" data-src="../assets/images/temp/overview/block-1.jpg"></div>
										</div>
									</div><!-- .thumbs -->
									
									<div class="full lazybg" data-src="../assets/images/temp/overview/ov9.jpg">
									</div>
									
								</div><!-- .photoviewer -->
								
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
				
				</div><!-- .sw -->
			</div>
			
			<div class="tab">
				<div class="sw">
				
					<div class="product-specifications-wrap">
				
						<div class="lazybg with-img">	
							<div class="imgmap" data-original='{"w":436,"h":650}'>
								<img src="../assets/images/temp/windows/specifications.jpg" alt="awning window">
							</div><!-- .imgmap-wrap -->
						</div><!-- .lazybg -->
						
						<div class="product-specifications">
							<ul>
								<li data-pos='{"x":389,"y":45}'>
									<p>Frame and sash are multi-chambered for added strength, stability and insulation.</p>
								</li>
								<li data-pos='{"x":293,"y":20}'>
									Heavy duty Stainless Steel hinges and tie bars will not rust or corrode.
								</li>
								<li data-pos='{"x":276,"y":65}'>
									Warm edge envirosealed windows™ include full 7/8" Insulating Glass with Low-E glass (1 1/8" triple glazing optional).
								</li>
								<li data-pos='{"x":363,"y":237}'>
									Heavy duty extruded aluminum screen bar with integral full perimeter pull rail and hidden positive action locking pin system.
								</li>
								<li data-pos='{"x":418,"y":479}'>
									Drying glazing system and a co-extruded flexible seal on sash exterior guards against leaks and maximize structural integrity.
								</li>
								<li data-pos='{"x":380,"y":538}'>
									Exclusive frame design keeps frame bulb seal out of sight.
								</li>
								<li data-pos='{"x":266,"y":558}'>
									Hidden drainage system eliminates unattractive holes or weep covers.
								</li>
								<li data-pos='{"x":2619,"y":600}'>
									Dual (one on each side) Muti-Point locks is a standard feature on all Awning windows.
								</li>
								<li data-pos='{"x":207,"y":631}'>
									 1/4" frame depth
								</li>
								<li data-pos='{"x":29,"y":561}'>
									Operators provide smooth and easy operation; operating handle has sleek contoured lines.
								</li>
								<li data-pos='{"x":8,"y":593}'>
									Fusion-welded corners on frame and sash maximize its strength and stability
								</li>
								<li>
									Triple-seal weather-stripping and a step-down frame design provide superior protection against air and water penetration.
								</li>
							</ul>
						
							<div class="extra">
								<h4 class="serif">Features Not Shown</h4>
								<ul>
									<li>Commercial style "blocked" glazing system keeps sash frame true and square.</li>
									<li>Flush mullion system eliminates notching interior trim.</li>
								</ul>
							</div><!-- .extra -->
						</div><!-- .product-specifications -->
					
					</div><!-- .product-specifications-wrap -->
					
				
				</div><!-- .sw -->
			</div>
			
			<div class="tab">
				<div class="sw">
				
					<div class="tab-wrapper">
					
						<div class="tab-controls">
							<div class="tab-control selected">Features</div>
							<div class="tab-control">Options</div>
						</div><!-- .tab-controls -->
						
						<div class="tab-holder">
							<div class="tab selected">
								
								<div class="product-features-wrap">
								
									<div class="extra">
									
										<div class="grid vcenter">
											<div class="col col-2">
												<div class="item center">
													<h4 class="serif">High Profile</h4>
													<p>Matches Casement and Awning profiles.</p>
												</div>
											</div>
											<div class="col col-2">
												<div class="item center">
													<h4 class="serif">High Profile</h4>
													<p>Matches Casement and Awning profiles.</p>
												</div>
											</div>
										</div>
										
										<hr />
										<br />
									
										<p>
											Frame and sash are multi-chambered for added strength, stability and insulation.
										</p>
										
										<p>
											Standard 7/8" Insulating Glass, optional 1 1/8" triple glazing.
										</p>
										
										<p>
											Commercial style "blocked" glazing system keeps frame true and square (not shown)
										</p>
										
										<p>
											Hidden exterior drainage system eliminates unattractive drain hole covers (not shown).
										</p>
										
										<div class="lazybg with-img">
											<img src="../assets/images/temp/windows/features-bottom.jpg">
										</div><!-- .lazybg -->
									
									</div><!-- .extra -->
								
									<ul class="features-list">
									
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/1.jpg">
											</div>
											<p>7/8" Insulating Glass</p>
										</li>
										
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/2.jpg">
											</div>
											<p>Die Cast Operating Handle</p>
										</li>
										
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/3.jpg">
											</div>
											<p>Multi-Chambered Frame and Sash</p>
										</li>
										
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/4.jpg">
											</div>
											
											<p>Stainless Steele Hinges and Tracks</p>
										</li>
										
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/5.jpg">
											</div>
											
											<p>Fusion-Welded Corners on Frame and Sash</p>
										</li>
										
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/6.jpg">
											</div>
											
											<p>Stainless Steel Tie Bars</p>
										</li>
										
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/7.jpg">
											</div>
											
											<p>Multi-Point Lock</p>
										</li>
										
										<li>
											<div class="lazybg with-img">
												<img src="../assets/images/temp/windows/8.jpg">
											</div>
											
											<p>Heavy Duty Extruded Aluminum Screen Bar</p>
										</li>
										
									</ul>
								
								</div><!-- .product-features -->
								
								
							</div><!-- .tab -->
							<div class="tab">
								
								<div class="article-body">
								
									<div class="grid eqh pad40 fill">
										<div class="col col-3 sm-col-2 xs-col-1">
											<div class="item">
											
												<h4>Argon Gas</h4>
												<p>This invisible, inert gas muffles sound and insulates much better than air.</p>
												
												<h4>Obscure Glass</h4>
												<p>This invisible, inert gas muffles sound and insulates much better than air.</p>
												
												<h4>Tinted Glass</h4>
												<p>Tinted in grey or bronze.</p>
												
												<h4>Internal Grills</h4>
												<p>5/8" White contoured grills come in Prarie, Colonial and Spoke designs.</p>
											
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 sm-col-2 xs-col-1">
											<div class="item">
											
												<h4>Simulated Divided Lite  Grills</h4>
												<p>Simulated Divided Lite grills beautifully capture the look of individual panes of glass. 
												Applied with special tape to interior and exterior glass surfaces, they are guaranteed to not delaminate for the the life of the window.</p>
												
												<h4>Colours: Solid PVC and Paint</h4>
												<p>Weathre Shore Windows come in two solid colours, White and Cream, as well as a painted exterior finish in a wide assortment of popular shades (white interior). 
												The paointed finish options feature a state-of-the-art colourizing system for unmatched fade resistance and colour retention.</p>
											
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col col-3 sm-col-2 xs-col-1">
											<div class="item">
											
												<h4>Simulated Divided Lite  Grills</h4>
												<ul>
													<li>On all solid colour (White and Cream) PVC components against blistering, peeling, flaking, cracking or fading. Painted windows carry a 12-year warranty.</li>
													<li>On all hardware against defects in material and manufacturing.</li>
												</ul>
												
												<h4>Lifetime Pro-rated Warranty</h4>
												<ul>
													<li>On all sealed insulating glass units against seal failure that results in the visible formation of moisture or dirt between the inner and outer panes of glass. Refer to the Weather Shore warranty certificate for complete </li>
												</ul>
											
											</div><!-- .item -->
										</div><!-- .col -->
											
										
									</div><!-- .grid -->
								
								</div><!-- .article-body -->
								
							</div><!-- .tab -->
						</div><!-- .tab-holder -->
					</div><!-- .tab-wrapper -->
				
				</div><!-- .sw -->
			</div>
			
			<div class="tab">
				<div class="sw">
					<div class="builder-callout-parent">
					
						<!-- 
							
							Elements with a class of 'launch-builder' launch the Window Build and Price overlay
							
							Take note of data-type and data-builderurl attributes
							
							data-type 
								the name that goes in the title bar (e.g. Awning Windows of the builder overlay
							
							data-builderurl 
								the url that returns the HTML that populates the body of the builder overlay
							
						-->
					
						<div class="launch-builder builder-callout dark-bg ar" data-ar="69" data-builderurl="./inc/i-build-price-content.php" data-type="Awning Window">
							<div class="ar-child">
								<div class="section-title">	
									<h3 class="title">Build your dream windows.</h3>
									<span class="subtitle">Use our window builder today.</span>
								</div><!-- .section-title -->
								
								<span class="launch">
									Launch the Builder Now
								</span><!-- .launch -->
							</div>
						</div><!-- .launch-builder -->
					</div><!-- .builder-callout-parent -->
				</div><!-- .sw -->
			</div>

		</div><!-- .tab-holder -->
	
	</div><!-- .tab-wrapper -->
	
</div><!-- .product -->
