<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Showcase</h1>
							
							<p>Page Subtitle.</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Showcase</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">
					
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->
				
		</div><!-- .sw -->
	</section>
	
	<section class="green-bg nopad showcase-section-nav">
		<div class="sw">
		
			<div class="grid eqh nopad">
				<div class="col-3 xs-col-1 col">
					<a href="#" class="item wsf windows">
						Windows Showcase
					</a>
				</div><!-- .col -->
				<div class="col-3 xs-col-1 col">
					<a href="#" class="item wsf doors">
						Doors Showcase
					</a>
				</div><!-- .col -->
				<div class="col-3 xs-col-1 col">
					<a href="#" class="item wsf exterior">
						Exterior Showcase
					</a>
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .greenbg -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>