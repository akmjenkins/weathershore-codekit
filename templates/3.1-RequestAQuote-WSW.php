<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Our Customer Experience</h1>
							
							<p>By your side from design to installation</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Contact</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
			<div class="main-body">
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
			</div>
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<p>Fill out the form below to contact us</p>
		
			<div class="grid pad40 eqh">
			
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<form action="/" method="post" class="body-form">
							<fieldset>
								
								<input type="text" name="name" placeholder="Full Name">
								<input type="email" name="email" placeholder="Email Address">
								<input type="tel" pattern="\d+" placeholder="Phone">
								<textarea name="message" cols="30" rows="10" placeholder="Tell us what you are looking for..."></textarea>
								
								<button class="button big" type="submit">Submit</button>
								
							</fieldset>
						</form><!-- .body-form -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-2 sm-col-1">
				
						<!-- 
							
							Elements with a class of 'launch-builder' launch the Window Build and Price overlay
							
							Take note of data-type and data-builderurl attributes
							
							data-type 
								the name that goes in the title bar (e.g. Awning Windows of the builder overlay
							
							data-builderurl 
								the url that returns the HTML that populates the body of the builder overlay
							
						-->
				
					<div class="item launch-builder builder-callout dark-bg" data-builderurl="./inc/i-build-price-content.php" data-type="Awning Window">
						<div>
							<div class="section-title">	
								<h3 class="title">Build your dream windows.</h3>
								<span class="subtitle">Use our window builder today.</span>
							</div><!-- .section-title -->
							
							<span class="launch">
								Launch the Builder Now
							</span><!-- .launch -->
						</div>
					</div><!-- .item -->
					
				</div><!-- .col -->
				
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section><!-- .grey-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>