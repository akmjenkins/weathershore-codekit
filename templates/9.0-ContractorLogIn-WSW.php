<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Contractor Log In</h1>
							
							<p>Sub Title</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Contractor Log In</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
			<div class="main-body">
			
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
				
			</div><!-- .main-body -->
		</div><!-- .sw -->
	</section>
	
	<section class="split-screen t dark-bg center collapse-1000">
		<div class="sw">
			<div class="grid ss eqh collapse-1000">
				<div class="col col-2">
					<div class="item">
					
						<div class="body-form-wrap">
					
							<h3>Log In</h3>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
							</p>
							
							<form action="/" class="body-form" method="post">
								<fieldset>
									
									<span class="field-wrap t-fa t-fa-abs fa-user">
										<input type="email" name="email" placeholder="E-mail">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa t-fa-abs fa-lock">
										<input type="password" name="password" placeholder="Password">
									</span><!-- .field-wrap -->
									
									<span class="block right alright">
										<p>Did you forget your password?</p>
										<a href="#" class="right inline">Forgot password?</a>
									</span><!-- .block -->
									
									<button type="submit" class="button big">Log In</button>
									
								</fieldset>
							</form><!-- .body-form -->
						
						</div><!-- .col-2-3 -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<div class="item">
					
						<div class="body-form-wrap">
					
							<h3>Sign Up</h3>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
							</p>
							
							<form action="/" class="body-form" method="post">
								<fieldset>
								
									<span class="field-wrap t-fa t-fa-abs fa-user">
										<input type="text" name="name" placeholder="Full Name">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa t-fa-abs fa-envelope">
										<input type="email" name="email" placeholder="E-mail">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa t-fa-abs fa-lock">
										<input type="password" name="password" placeholder="Password">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa t-fa-abs fa-lock">
										<input type="password" name="cpassword" placeholder="Confirm Password">
									</span><!-- .field-wrap -->
									
									<a href="#" class="right inline">Forgot password?</a>
									
									<button type="submit" class="button big">Sign Up</button>
									
								</fieldset>
							</form><!-- .body-form -->
						
						</div><!-- .col-2-3 -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .split-screen -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>