<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Windows Showcase</h1>
							
							<p>Page Subtitle.</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Showcase</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
		
			<div class="section-title l">
				<h2>Featured</h2>
				<span class="subtitle">Lorem ipsum dolor sit amet.</span>
			</div><!-- .section-title -->
			
			<a class="featured-block bounce" href="#">
			
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/images/temp/featured-block.jpg">
						<img src="../assets/images/temp/featured-block.jpg">
					</div>
				</div><!-- .img-wrap -->
				
				<div class="content-wrap">
					
					<div class="section-title l">
						<h3>Euismod Bibendum Laoreet Proin Gavida</h3>
						<span class="subtitle">Lorem ipsum dolor sit amet.</span>
					</div><!-- .section-title -->
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
					</p>
					
					<span class="button green">Read More</span>
					
				</div><!-- .content-wrap -->
			
			</a><!-- .featured-block -->
		
		</div><!-- .sw -->
	</section>
	
	<section>
		
		<div class="filter-area">
		
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
					<div class="count">
						<span class="num">10</span>
						Pictures Found
					</div><!-- .count -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
				<div class="sw">
					
					<div class="grid">
					
						<div class="col-3 col sm-col-2 xs-col-1">
							<div class="item ar bounce" data-ar="60">
							
								<!-- 
									the data-src is for .lazybg - it's the thumbnail
									the href is for magnific popup, it's the full size image
								-->
							
								<a class="img lazybg mpopup ar-child" data-gallery="gallery_identifier" href="../assets/images/temp/overview/ov4.jpg" data-src="../assets/images/temp/overview/ov4.jpg"></a>
							</div>
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2 xs-col-1">
							<div class="item ar bounce" data-ar="60">
								<a class="img lazybg mpopup ar-child" data-gallery="gallery_identifier" data-src="../assets/images/temp/overview/ov7.jpg" href="../assets/images/temp/overview/ov7.jpg"></a>
							</div>
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2 xs-col-1">
							<div class="item ar bounce" data-ar="60">
								<a class="img lazybg mpopup ar-child" data-gallery="gallery_identifier" data-src="../assets/images/temp/overview/block-1.jpg" href="../assets/images/temp/overview/block-1.jpg"></a>
							</div>
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2 xs-col-1">
							<div class="item ar bounce" data-ar="60">
								<a class="img lazybg mpopup ar-child" data-gallery="gallery_identifier" data-src="../assets/images/temp/overview/ov10.jpg" href="../assets/images/temp/overview/ov10.jpg"></a>
							</div>
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2 xs-col-1">
							<div class="item ar bounce" data-ar="60">
								<a class="img lazybg mpopup ar-child" data-gallery="gallery_identifier" data-src="../assets/images/temp/overview/ov9.jpg" href="../assets/images/temp/overview/ov9.jpg"></a>
							</div>
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2 xs-col-1">
							<div class="item ar bounce" data-ar="60">
								<a class="img lazybg mpopup ar-child" data-gallery="gallery_identifier" data-src="../assets/images/temp/overview/ov8.jpg" href="../assets/images/temp/overview/ov8.jpg"></a>
							</div>
						</div><!-- .col -->

					</div><!-- .grid -->

					
				</div><!-- .sw -->
			</div><!-- .filter-content -->
			
		</div><!-- .filter-area -->
		
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>