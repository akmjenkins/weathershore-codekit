var ns = 'JAC-WS';
window[ns] = {};

// debounce - used by a bunch of things
// @codekit-append "scripts/debounce.js";

// @codekit-append "scripts/components/anchors.external.popup.js"; 
// @codekit-append "scripts/components/standard.accordion.js";
// @codekit-append "scripts/components/custom.select.js";
// @codekit-append "scripts/components/magnific.popup.js";
// @codekit-append "scripts/components/responsive.video.js";
// @codekit-append "scripts/components/tabs.js";

// Used in swipe.srf.js
// @codekit-append "scripts/components/swipe.js"; 
// @codekit-append "scripts/components/srf.js"; 
// @codekit-append "scripts/swipe.srf.js"; 

// @codekit-append "scripts/aspect.ratio.js";
// @codekit-append "scripts/lazy.images.js";
// @codekit-append "scripts/search.js";

// @codekit-append "scripts/product.nav.js";
// @codekit-append "scripts/build.price.js";

// @codekit-append "scripts/blocks.js";
// @codekit-append "scripts/nav.js
// @codekit-append "scripts/hero.js

(function(context) {	

		var $document = $(document);
		
		
		$document
			.on('click','.inline-video',function(e) {
				var 
					el = $(this),
					src = el.data('src');
					
				if(src.length && !el.hasClass('inline-video-playing')) {
					el
						.addClass('inline-video-playing')
						.append('<iframe src="'+src+'"/>');
				}
				
			})
			.on('tabChanged',function(e,el) {
			
				$document.trigger('updateTemplate');
				
			});
		
		(function() {
			$('.video-container').each(function() {
				var 
					el = $(this),
					ratio = el.data('ratio');
					
					el.css({paddingBottom:ratio+'%'});
			});
		}());
		
		//tooltipster
		(function() {
		
			var tooltipsterMethods = {
				update: function() {
					$('.tooltipster')
						.filter(function() { return !$(this).hasClass('tooltipstered'); })
						.each(function() {
							var 
								el = $(this),
								opts = el.data();
								
							el.tooltipster({
								content: $(opts.selector).html(),
								contentAsHTML:true
							});
					});
				}
			};
			
			tooltipsterMethods.update();
			$document.on('updateTemplate.tooltipster',function() {
				tooltipsterMethods.update();
			});
		
		}());	

}(window[ns]));