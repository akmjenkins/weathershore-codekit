//standard accordion
(function(context) {
	var opts;

	$(document)
		.on('click','div.accordion-item-handle',function(e) {
			var 
				el = $(this).parent(),
				acc = el.closest('div.accordion');
				
			e.preventDefault();
			
			if(acc.hasClass('inactive') || el.hasClass('inactive')) { return; }
			
			opts = {duration:450};
			
			if(el.hasClass('expanded')) {
				el.removeClass('expanded').children('div.accordion-item-content').stop().slideUp(opts);
			} else {
				el.addClass('expanded').children('div.accordion-item-content').stop().slideDown(opts);
			}
			
			acc.hasClass('allow-multiple') || el.siblings().removeClass('expanded').children('div.accordion-item-content').stop().slideUp(opts);
			
			//no bubble;
			return false;
			
		}).each(function() {
			$(this).find('div.expanded').children('div.accordion-item-content').slideDown(opts);
		});
	
}(window[ns]));