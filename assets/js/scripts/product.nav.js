(function(context) {

	var
		$productContent = $('div.product-content'),
		$nav = $('div.product-nav'),
		LOADING_CLASS = 'loading',
		LOADED_CLASS = 'loaded',
		SELECTED_CLASS = 'selected',
		loadReq,
		methods = {
		
			selectImgMapSpec: function(spec,deselect) {
				
				if(deselect) {
					spec
						.removeClass('selected')
						.data('linked')
						.removeClass('selected');
						return;
				}
				
				spec
					.addClass('selected')
					.data('linked')
					.addClass('selected')
					.siblings()
					.removeClass('selected')
					.end()
					.end()
					.siblings()
					.removeClass('selected');
				
			},
		
			buildImgMap: function() {
			
				var 
					self = this,
					$imgMap = $productContent.find('div.imgmap'),
					original = $imgMap.data('original'),
					specifications = $productContent.find('div.product-specifications li').filter(function() { return !!$(this).data('pos'); });
					
					specifications.each(function() {
						var 
							el = $(this),
							pos = el.data('pos'),
							span = $('<span class="s"/>').css({
								top:((pos.y/original.h)*100)+'%',
								left:((pos.x/original.w)*100)+'%',
							});
						
						$imgMap.append(span);
						el.data('linked',span);
						
						span
							.on('mouseenter',function() {
								self.selectImgMapSpec(el);
							})
							.on('mouseleave',function() {
								self.selectImgMapSpec(el,true);
							});
						
					});
					
					specifications
						.on('mouseenter',function() {
							self.selectImgMapSpec($(this));
						})
						.on('mouseleave',function() {
							self.selectImgMapSpec($(this),true);
						});
				
			
			},
		
			load: function(url) {
				var self = this;
				$productContent
					.removeClass(LOADED_CLASS)
					.addClass(LOADING_CLASS);
			
				loadReq && loadReq.abort();
				setTimeout(function() {
				loadReq = $.get(url)
					.done(function(r) {
						$productContent
							.html(r)
							.removeClass(LOADING_CLASS)
							.addClass(LOADED_CLASS);
							
						$(document).trigger('updateTemplate');
						self.buildImgMap();
					})
					.fail(function() {
					
					});
				},2000);
					
				
			}
		
		};
		
	//listeners
	$(document)
		.on('click','.product-nav .nav-element',function() {
			$(this)
				.addClass(SELECTED_CLASS)
				.siblings()
				.removeClass(SELECTED_CLASS);
			methods.load(this.href);
			
			return false;
		})
		.on('click','.product-nav .nav-controls button',function() {
			var 
				el = $(this),
				container = $('div.inner-product-nav'),
				w = container.find('a').outerWidth(),
				dir = el.hasClass('next') ? 1 : -1;
				
				container.animate({
					scrollLeft: '+='+ (w*dir),
				},200);
		})

}(window[ns]));