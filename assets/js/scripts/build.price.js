(function(context) {

	var 
		$body = $('body'),
		$bpw = $('.build-price-window'),
		SELECTED_CLASS = 'selected',
		LOADING_CLASS = 'loading',
		SHOW_CLASS = 'show-bp',
		req,
		methods = {
	
			load: function(type,url) {
			
				this.show(true);
			
				$bpw
					.addClass(LOADING_CLASS)
					.find('div.title-bar span.type').html(type);

				req && req.abort();
				setTimeout(function() {
				
					req = $.get(url)
						.done(function(r) {
							$bpw
								.find('div.bp-content')
								.html(r);
								
								$(document).trigger('updateTemplate');
								
								//force the swiper to be rebuilt twice - it's in a flex container
								setTimeout(function() {
									$bpw
										.removeClass(LOADING_CLASS)
										.find('div.swipe')
										.data('swipe')
										.setup();
								},200);
								$bpw.trigger('swipeChanged',[0,null]);

						})
						.fail(function() {
							//show a message
						});
					
				},2000);
			
			},
	
			isShowing: function() {
				return $body.hasClass(SHOW_CLASS);
			},
			
			toggle: function() {
				this.show(!this.isShowing());
			},
			
			show: function(show) {
				if(show) {
					$bpw.css({display:'block'});
					setTimeout(function() { $body.addClass(SHOW_CLASS); },300);
				} else {
					$body.removeClass(SHOW_CLASS);
					setTimeout(function() { $bpw.css({display:'none'}); },300);
				}
			}
	
		}

	//listeners
	$(document)
		.on('click','.launch-builder',function() {
			var el = $(this);
			methods.load(el.data('type'),el.data('builderurl'));
		})
		.on('click','.build-price-window .close',function() {
			methods.show(false);
		}).on('keyup',function(e) {
			if(e.keyCode === 27 && methods.isShowing() && methods.show(false)) {
				return false;
			}		
		});
		
	$bpw
		.on('change','div.selector select',function() {
			$bpw
				.find('div.swipe')
				.data('swipe')
				.slide(this.selectedIndex);
		})
		.on('swipeChanged',function(e,i,div) {
			$bpw
				.find('.bp-nav button')
				.removeClass(SELECTED_CLASS)
				.eq(i)
				.addClass(SELECTED_CLASS);
				
			var $selector = $bpw.find('div.selector');
			$selector
				.find('select')[0]
				.selectedIndex = i;
			
			$selector.data('selector').update();
		})
		.on('click','button.next',function() {
			$bpw
				.find('div.swipe')
				.data('swipe')
				.next();
		})
		.on('click','.bp-nav button',function() {
			var buttons = $('div.bp-nav button');
			$bpw
				.find('div.swipe')
				.data('swipe')
				.slide(buttons.index(this));
		})
		.on('click','.option',function() {
			$(this).addClass('selected').siblings().removeClass('selected');
		});
		



	//no public API
	return {};

}(window[ns]));